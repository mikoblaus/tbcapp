import {
    ADD_NEW_USER_REQUEST,
    GET_USER_REQUEST,
    EDIT_USER_REQUEST
} from "./actionTypes"
import {notification} from 'antd';

const addNewUserError = () => {
    notification['error']({
        message: 'Error',
        description: 'The user id already exists',
    });
};

const addNewUserSuccess = () => {
    notification['success']({
        message: 'Success',
        description: 'User successfully created!',
    });
};

const editUser = () => {
    notification['success']({
        message: 'Success',
        description: 'Data successfully renewed',
    });
};


const INIT_STATE = {
    array: [
        {
            firstName: "George",
            lastName: "Walsh",
            ID: "12345678901",
            gender: "Male",
            birthDate: "2001-05-24",
            placeOfBirth: "Hartford",
            address: "Hartford 13"
        },
        {
            firstName: "George2",
            lastName: "Walsh2",
            ID: "12345678902",
            gender: "Male",
            birthDate: "2001-05-24",
            placeOfBirth: "Hartford",
            address: "Hartford 13a"
        },
        {
            firstName: "George3",
            lastName: "Walsh3",
            ID: "12345678903",
            gender: "Female",
            birthDate: "2001-05-24",
            placeOfBirth: "Hartford",
            address: "Hartford 13b"
        },
    ],
    user: null
}

const Users = (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_USER_REQUEST:
            return {
                ...state,
                user: state.array[action.payload]
            }
        case EDIT_USER_REQUEST:
            state.array[action.payload.key] = action.payload.data
            editUser()
            return {
                ...state,
            }
        case ADD_NEW_USER_REQUEST:
            const users = state.array.find(
                users => users.ID.toString() === action.payload.ID.toString()
            )
            if (!users) {
                addNewUserSuccess()
                return {
                    ...state,
                    array: [...state.array, action.payload]
                }
            } else {
                addNewUserError()
                return {
                    ...state,
                }
            }


        default:
            return state
    }
}

export default Users
