import { Route, Redirect } from "react-router-dom";

const Authmiddleware = ({
                            component: Component,
                            layout: Layout,
                            isAuthProtected,
                            ...rest
                        }) => (
    <Route
        {...rest}
        render={props => {
            return (
                <Component {...props} />
            );
        }}
    />
);


export default Authmiddleware;
