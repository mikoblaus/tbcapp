import { combineReducers } from "redux"
import Users from "./User/reducer"

const rootReducer = combineReducers({
    Users
})

export default rootReducer
