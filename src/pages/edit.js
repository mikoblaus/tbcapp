import React, {useState, useEffect} from 'react';
import moment from 'moment';
import {yupResolver} from '@hookform/resolvers/yup';
import {Container, TextField, MenuItem} from '@mui/material';
import {useForm, SubmitHandler, Controller} from 'react-hook-form';
import {Link, withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {addNewUser, editUser, getUser} from "../store/User/actions";

import {Schema} from "../validations/Registration";

const Edit = (props) => {
    const dispatch = useDispatch();
    const {Users} = props
    const [user, setUser] = useState(null);
    const {
        reset,
        control,
        handleSubmit,
        formState: {errors},
    } = useForm({
        resolver: yupResolver(Schema),
    });

    const onSubmit = (data) => {
        let item = {
            key: props.match.params.key,
            data
        }

        dispatch(editUser(item))
    }

    useEffect(() => {

        dispatch(getUser(props.match.params.key))
        // simulate async api call with set timeout

        setUser(Users.user)
    }, [Users.user]);

    useEffect(() => {
        // reset form with user data
        reset(user);
    }, [user]);


    return (
        <React.Fragment>
            <Container
                sx={{
                    mt: '1rem'
                }}
            >
                <Link to="/">
                    Back
                </Link>

                <hr/>

                <form
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Controller
                        name="firstName"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                label="First Name"
                                variant="outlined"
                                defaultValue={"asdf"}
                                error={!!errors.firstName}
                                helperText={errors.firstName ? errors.firstName?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />
                    <br/>
                    <Controller
                        name="lastName"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Last Name"
                                variant="outlined"
                                error={!!errors.lastName}
                                helperText={errors.lastName ? errors.lastName?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />


                    <br/>
                    <Controller
                        name="ID"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Id"
                                variant="outlined"
                                error={!!errors.ID}
                                helperText={errors.ID ? errors.ID?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />
                    <br/>
                    <Controller
                        name="gender"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                select
                                id="gender"
                                label="Gender"
                                variant="outlined"
                                error={!!errors.gender}
                                helperText={errors.gender ? errors.gender?.message : ''}
                                fullWidth
                                margin="dense"
                            >

                                <MenuItem value={'Male'}>Male</MenuItem>
                                <MenuItem value={'Female'}>Female</MenuItem>
                            </TextField>
                        )}
                    />
                    <br/>
                    <Controller
                        name="birthDate"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                label="Birthday"
                                type="date"
                                fullWidth
                                margin="dense"

                                error={!!errors.birthDate}
                                helperText={errors.birthDate ? errors.birthDate?.message : ''}
                                inputProps={{max: moment().subtract(1, "days").format("YYYY-MM-DD")}}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        )}
                    />

                    <Controller
                        name="placeOfBirth"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Place of birth"
                                variant="outlined"

                                error={!!errors.placeOfBirth}
                                helperText={errors.placeOfBirth ? errors.placeOfBirth?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />


                    <Controller
                        name="address"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Address"
                                variant="outlined"
                                error={!!errors.address}
                                helperText={errors.address ? errors.address?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />


                    <input type="submit"/>
                </form>
            </Container>
        </React.Fragment>
    )
}

const mapStateToProps = state => {
    const {
        Users
    } = state
    return {
        Users
    }
};
export default withRouter(connect(mapStateToProps)(Edit));
