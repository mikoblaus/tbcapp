import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Authmiddleware from "./routes/middleware/Authmiddleware";

import { userRoutes } from "./routes/allRoutes";


import "./assets/scss/antd.css"

function App() {
    return (
        <BrowserRouter>
            <Switch>
                {userRoutes.map((route, idx) => (
                    <Authmiddleware
                        path={route.path}
                        component={route.component}
                        key={idx}
                        exact
                    />
                ))}
            </Switch>
        </BrowserRouter>
    );
}

export default App;
