import * as React from 'react';
import Moment from 'react-moment';
import moment from 'moment';
import {yupResolver} from '@hookform/resolvers/yup';
import {Container, TextField, MenuItem} from '@mui/material';
import {useForm, SubmitHandler, Controller} from 'react-hook-form';

import {Link} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {addNewUser} from "../store/User/actions";


import {Schema} from "../validations/Registration";


const Add = () => {
    const dispatch = useDispatch();
    const {
        reset,
        control,
        handleSubmit,
        formState: {errors},
    } = useForm({
        resolver: yupResolver(Schema),
    });

    const onSubmit = (data) => {
        dispatch(addNewUser(data))
    }

    return (
        <React.Fragment>
            <Container
                sx={{
                    mt: '1rem'
                }}
            >
                <Link to="/">
                    Back
                </Link>

                <hr/>

                <form
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Controller
                        name="firstName"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                label="First Name"
                                variant="outlined"
                                error={!!errors.firstName}
                                helperText={errors.firstName ? errors.firstName?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />
                    <br/>
                    <Controller
                        name="lastName"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Last Name"
                                variant="outlined"
                                error={!!errors.lastName}
                                helperText={errors.lastName ? errors.lastName?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />


                    <br/>
                    <Controller
                        name="ID"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Id"
                                variant="outlined"
                                error={!!errors.ID}
                                helperText={errors.ID ? errors.ID?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />
                    <br/>
                    <Controller
                        name="gender"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                select
                                id="gender"
                                label="Gender"
                                variant="outlined"
                                error={!!errors.gender}
                                helperText={errors.gender ? errors.gender?.message : ''}
                                fullWidth
                                margin="dense"
                            >

                                <MenuItem value={'Male'}>Male</MenuItem>
                                <MenuItem value={'Female'}>Female</MenuItem>
                            </TextField>
                        )}
                    />
                    <br/>
                    <Controller
                        name="birthDate"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                label="Birthday"
                                type="date"
                                fullWidth
                                margin="dense"
                                InputLabelProps={{ shrink: true, required: true }}
                                error={!!errors.birthDate}
                                helperText={errors.birthDate ? errors.birthDate?.message : ''}
                                inputProps={{max: moment().subtract(1, "days").format("YYYY-MM-DD")}}

                            />
                        )}
                    />

                    <Controller
                        name="placeOfBirth"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Place of birth"
                                variant="outlined"
                                error={!!errors.placeOfBirth}
                                helperText={errors.placeOfBirth ? errors.placeOfBirth?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />


                    <Controller
                        name="address"
                        control={control}
                        defaultValue=""
                        render={({field}) => (
                            <TextField
                                {...field}
                                type="text"
                                label="Address"
                                variant="outlined"
                                error={!!errors.address}
                                helperText={errors.address ? errors.address?.message : ''}
                                fullWidth
                                margin="dense"
                            />
                        )}
                    />
                    <input type="submit"/>
                </form>
            </Container>
        </React.Fragment>
    )
}

export default Add
