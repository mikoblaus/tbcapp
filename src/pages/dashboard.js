import React, {useEffect} from "react";
import {Container} from '@mui/material';
import MUIDataTable from "mui-datatables";
import {connect, useDispatch} from "react-redux";
import {withRouter} from "react-router-dom";
import { Link } from "react-router-dom"
import { useHistory } from "react-router-dom";




const Dashboard = (props) =>{
    const history = useHistory();
    const {Users} = props
    const columns = [
        {
            name: "firstName",
            label: "Name",
            options: {
                filter: true,
                sort: true,
            }
        },
        {
            name: "lastName",
            label: "Company",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "ID",
            label: "ID",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "gender",
            label: "Gender",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "birthDate",
            label: "Birth Date",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "placeOfBirth",
            label: "Place Of Birth",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "address",
            label: "Address",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "Action",
            options: {
                filter: false,
                sort: false,
                empty: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <>
                            <button
                                onClick={(e) => {
                                    e.stopPropagation();
                                    history.push(`/edit/${tableMeta.rowIndex}`);
                                }}
                            >
                                Edit
                            </button>
                        </>
                    );
                }
            }
        }
    ];

    const options = {
        filterType: 'checkbox',
    };

    return (
        <React.Fragment>

            <Container
                sx={{
                    mt: '1rem'
                }}
            >
                <Link to="/add"  >
                   Add new user
                </Link>

                <hr/>

                <MUIDataTable
                    title={"Users list"}
                    data={Users.array}
                    columns={columns}
                    options={options}
                />

            </Container>
        </React.Fragment>
    )
}


const mapStateToProps = state => {
    const {
        Users
    } = state
    return {
        Users
    }
};
export default withRouter(connect(mapStateToProps)(Dashboard));

