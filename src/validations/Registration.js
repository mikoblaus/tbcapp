import * as yup from 'yup';

export const Schema = yup.object().shape({
    firstName: yup.string().required('First Name is a required field'),
    lastName: yup.string().required('Last Name is a required field'),
    ID: yup.string().required()
        .matches(/^[0-9]+$/, "Must be only digits")
        .min(11, 'Must be exactly 11 digits')
        .max(11, 'Must be exactly 11 digits'),
    gender: yup.string().required('Gender is a required field'),
    birthDate: yup.string().required('Birth Date is a required field'),
    placeOfBirth: yup.string().required('Place Of Birth is a required field'),
    address: yup.string().required('Address is a required field'),
});
