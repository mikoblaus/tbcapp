import {GET_USER_REQUEST, ADD_NEW_USER_REQUEST, EDIT_USER_REQUEST} from "./actionTypes";

export const getUser = (data) => ({
    type: GET_USER_REQUEST,
    payload : data
})

export const addNewUser = (data) => ({
    type: ADD_NEW_USER_REQUEST,
    payload : data
})

export const editUser = (data) => ({
    type: EDIT_USER_REQUEST,
    payload : data
})
