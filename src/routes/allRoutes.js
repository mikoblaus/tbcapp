import React from "react"
import {Redirect} from "react-router-dom"

import Dashboard from "../pages/dashboard"
import Add from "../pages/add"
import Edit from "../pages/edit"

const userRoutes = [
    {path: "/", component: Dashboard},
    {path: "/add", component: Add},
    {path: "/edit/:key", component: Edit},

    {path: "/", exact: true, component: () => <Redirect to="/"/>},
    {path: "*", component: Dashboard},
]

export {userRoutes}
